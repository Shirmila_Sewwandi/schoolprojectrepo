package examRegistration;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JTextPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import java.awt.Canvas;

public class login extends JFrame implements ActionListener {
	private JTextField userNameField;
	private JPasswordField passwordField;
	JButton resetbtn = new JButton("Reset");
	JButton btnLogin = new JButton("Login");

	public login() {
		initialize();
	}

	private void initialize() {
		
		this.setVisible(true);
		this.setBounds(100, 100, 450, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.getContentPane().setLayout(null);
		this.setSize(500, 250);
		
		btnLogin.setFocusable(false);

		btnLogin.setFont(new Font("Comic Sans", Font.BOLD, 14));
		btnLogin.setBounds(32, 139, 200, 50);
		
		getContentPane().add(btnLogin);
		btnLogin.addActionListener(this);
		
		userNameField = new JTextField();
		userNameField.setBounds(274, 37, 200, 30);
		getContentPane().add(userNameField);
		userNameField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(274, 87, 200, 30);
		getContentPane().add(passwordField);
		passwordField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Username");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(103, 35, 100, 30);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_1.setBounds(103, 85, 100, 30);
		getContentPane().add(lblNewLabel_1);
		
		
		resetbtn.setFont(new Font("Dialog", Font.BOLD, 14));
		resetbtn.setBounds(274, 139, 200, 50);
		resetbtn.addActionListener(this);
		getContentPane().add(resetbtn);
		this.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==resetbtn) {
			userNameField.setText("");
			passwordField.setText("");
		}
		if(e.getSource()==btnLogin) {
			String userID = userNameField.getText();
			String password = String.valueOf(passwordField.getPassword());
			if(userID.equals("") || password.equals("")) {
				JOptionPane.showMessageDialog(null, "Please enter the details!!");
			}
		else {
			String userid = userNameField.getText();
			String Password = String.valueOf(passwordField.getPassword());
			
			ResultSet rs;
			SqlConnection sql = new SqlConnection();
			rs = sql.SaveToDataBase(userid, Password);
			try {
				if(rs.next()) {
					int usertype = rs.getInt("usertype");
					if(usertype == 0) {
						JOptionPane.showMessageDialog(null,"Welcome !!!");
						Register register = new Register();
						register.showResult(userid);
					}
					else {
						JOptionPane.showMessageDialog(null,"Hello admin");
					}
					this.dispose();
				}
				else {
					JOptionPane.showMessageDialog(null,"Incorrect Username Or Password","Login Failed",2);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
			
		}
		
	}
}
